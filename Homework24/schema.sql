create table product
(
    id          serial primary key,
    description varchar(30),
    price       integer,
    count       integer
);
create table customer
(
    id         serial primary key,
    first_name varchar(20),
    last_name  varchar(20)
);

create table booking
(
    owner_id      integer,
    foreign key (owner_id) references product (id),
    host_id       integer,
    foreign key (host_id) references customer (id),
    data          varchar(20),
    count_product integer
);

insert into product(description, price, count)
values ('Ноутбук', '20000', 4);
insert into product(description, price, count)
values ('Моноблок', '35000', 12);
insert into product(description, price, count)
values ('Стационарный ПК', '10000', 2);
insert into product(description, price, count)
values ('Игровая консоль', '7000', 1);

insert into customer(first_name, last_name)
values ('Николай', 'Иванов');
insert into customer(first_name, last_name)
values ('Евгений', 'Петров');
insert into customer(first_name, last_name)
values ('Александр', 'Васиков');
insert into customer(first_name, last_name)
values ('Анатолий', 'Вассерман');

insert into booking(owner_id, host_id, data, count_product)
values ('1', '2', '08.12.2021', 1);
insert into booking(owner_id, host_id, data, count_product)
values ('1', '3', '08.12.2021', 3);
insert into booking(owner_id, host_id, data, count_product)
values ('1', '4', '08.12.2021', 1);
insert into booking(owner_id, host_id, data, count_product)
values ('2', '2', '08.12.2021', 4);


--получим количество товаров заказанное определенным человеком
select first_name, (select count(*) from booking)
from customer
where id = 2;

--получим количество техники больше или равное 2
select description, count
from product
where count >= 2;

--объеденим 3 таблицы во временную
select *
from booking;
select *
from product;
select *
from customer;
--распечатаем столбцы из разных таблиц
select first_name, last_name, description, count_product
from customer c
         join booking b on c.id = b.host_id
         join product p on p.id = b.owner_id;
--Получим имена тех кто приобрел Ноутбки
select first_name, last_name, description, count_product
from customer c
         join booking b on c.id = b.host_id
         join product p on p.id = b.owner_id
where description = 'Ноутбук';
