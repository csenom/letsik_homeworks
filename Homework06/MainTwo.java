import java.util.Arrays;

public class MainTwo {
    public static void main(String[] args) {
        int[] array = new int[]{34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        //отсортеруем по возрастанию
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
        //Просто перевернем массив
        int[] arr = new int[array.length];
        for (int i = array.length - 1; i > 0; ) {
            for (int j = 0; j < array.length; j++) {
                arr[j] = array[i];
                i--;
            }
        }
        System.out.println(Arrays.toString(arr));

    }
}


