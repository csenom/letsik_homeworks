import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int count;
        int item;
        int[] array;
        Scanner input = new Scanner(System.in);
        while (true) {
            try {
                array = new int[10];
                for (int i = 0; i < array.length; i++) {
                    array[i] = (int) Math.round((Math.random() * 30));// генерируем числа в массиве
                }
                System.out.println(Arrays.toString(array));
                System.out.println("Введите число, индекс которго требуется уточнить?");
                item = input.nextInt();

                for (count = 0; count < 10; count++) {
                    if (array[count] == item) {
                        System.out.println("Индексом числа " + item + " является " + count);
                        if (array[count] > 1) {
                            System.out.println("Число в массиве повторяется несколько раз");
                            /*Если честно образовался вопрос, как выводить это сообщение только один раз,
                            перепробовал несколько методов, но как то не сошлось*/

                        }
                    }


                    if (count == 10) {
                        System.out.println("Число " + item + " не найдено в массиве");
                        System.out.println(-1);
                    }
                }
            } catch (InputMismatchException e) {
                System.out.println("Вы не верно ввели число, попробуйте еще раз:");
            }
        }
    }
}