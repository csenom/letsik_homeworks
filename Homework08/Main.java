import java.util.*;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        HashMap<String, Integer> test = new HashMap<String, Integer>();
        test.put("Илья", 90);
        test.put("Алексей", 75);
        test.put("Дмитрий", 60);
        test.put("Анатолий", 99);
        test.put("Валерий", 15);
        test.put("Владимир", 35);
        test.put("Николай", 60);
        test.put("Марк", 26);
        test.put("Станислав", 52);
        test.put("Сергей", 120);
        test.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue())
                .forEach(System.out::println);
    }
}
