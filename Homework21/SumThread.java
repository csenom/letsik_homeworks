public class SumThread extends Thread {
    private int from;
    private int to;
    private final int nunOfTrend;

    public SumThread(int from, int to, int nunOfTrend) {
        this.from = from;
        this.to = to;
        this.nunOfTrend = nunOfTrend;
    }

    @Override
    public void run() {

        for (int i = from; i <= to; i++) {
            Main.sums[nunOfTrend] += Main.array[i];
        }
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }
}
