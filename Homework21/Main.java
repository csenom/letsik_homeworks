import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;


public class Main {

    public static int array[];

    public static int sums[];

    public static void main(String[] args) throws InterruptedException {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ввести размер массива");
        int numbersCount = scanner.nextInt();
        System.out.println("Ввести число потоков");
        int threadsCount = scanner.nextInt();

        array = new int[numbersCount];
        sums = new int[threadsCount];

        // заполняем случайными числами
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        final int[] realSum = {0};

        for (int k : array) {
            realSum[0] += k;
        }

        // для 2 000 000 -> 98996497, 98913187
        System.out.println("realSum - " + realSum[0]);
        // начало кода домашней работы

        ArrayList<SumThread> threads = new ArrayList<>();
        int lengthArrParts = array.length / threadsCount;

        int start = 0;
        int end = lengthArrParts;

        for (int j = 0; j < threadsCount ; j++)   {
            if ((array.length - end -1 )>= (array.length % threadsCount) ) {
                threads.add(new SumThread(start, end, j));
                start = end + 1;
                end = start + lengthArrParts;
            }
            else {
                threads.add(new SumThread(start, array.length - 1, j));
            }
        }

        for (int i = 0; i < threadsCount; i++){
            SumThread ttt = threads.get(i);

        }

        for (int j = 0; j < threadsCount; j++) {
            threads.get(j).start();
            try {
                threads.get(j).join();

            } catch (InterruptedException e) {
                throw new InterruptedException();
            }
        }
        int byThreadSum = 0;

        for (int i = 0; i < threadsCount; i++) {
            byThreadSum += sums[i];
        }

        System.out.println("byThreadSum - "+ byThreadSum);

    }
}
