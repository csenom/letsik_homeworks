public class Circle extends Ellipse implements Moving {


    public Circle(int coordinateX, int coordinateY, int x) {
        super(coordinateX, coordinateY, x, x);
        this.x = x;

    }

    @Override
    public void moveToThisCord(int setCoordX, int setCoordY) {
        this.x = setCoordX;
        this.coordinateY = setCoordY;
        System.out.println("Переместили круг на следующие координаты: x = " + this.x + ", y = " + this.coordinateY);
    }

    @Override
    public double getPerimeter() {
        double perimetrCircle;
        perimetrCircle = 2 * Math.PI * x;
        return perimetrCircle;
    }


}
