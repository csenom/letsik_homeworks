public class Ellipse extends Figure {
    protected int coordinateX;
    protected int coordinateY;

    public Ellipse(int coordinateX, int coordinateY, int x, int y) {
        super(coordinateX, coordinateY);
        this.x = x;
        this.y = y;
    }

    @Override
    public double getPerimeter() {
        double a;
        double xSquare;
        double ySquare;
        double perimeterEllipse;
        xSquare = x * x;
        ySquare = y * y;
        a = (xSquare + ySquare) / 2;
        perimeterEllipse = 2 * Math.PI * Math.sqrt(a);
        return perimeterEllipse;
    }
}
