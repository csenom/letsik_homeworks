public class Main {

    public static void main(String[] args) {

        Moving[] movingFigures = new Moving[2];

        Circle circle = new Circle(10, 10, 2);
        Square square = new Square(10, 10, 2);

        movingFigures[0] = circle;
        movingFigures[1] = square;

        for (int i = 0; i < movingFigures.length; i++) {
            movingFigures[i].moveToThisCord(10, 10);
        }
        if (circle.getX() == square.getX() && circle.getY() == square.getY()) {
            String nameSquare = square.getClass().getSimpleName();
            String nameCircle = circle.getClass().getSimpleName();
            System.out.printf("Координаты %s и %s равны по X = %d и по Y = %d\n", nameCircle, nameSquare, circle.getX(), circle.getY());
            System.out.println("Тест пройден");
        } else {
            System.out.println("Тест не пройден");
        }

    }
}
