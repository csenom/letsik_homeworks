public abstract class Figure {
    protected int x;
    protected int y;
    protected int coordinateX;
    protected int coordinateY;


    public Figure(int coordinateX, int coordinateY) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    public int getX() {
        return coordinateX;
    }

    public int getY() {
        return coordinateY;
    }


    public double getPerimeter() {
        return 0;
    }
}
