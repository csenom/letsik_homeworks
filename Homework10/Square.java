public class Square extends Rectangle implements Moving {
    public Square(int coordinateX, int coordinateY, int x) {
        super(x, x, coordinateX, coordinateY);
        this.x = x;
    }


    @Override
    public void moveToThisCord(int setCoordinateX, int setCoordinateY) {
        this.coordinateX = setCoordinateX;
        this.coordinateY = setCoordinateY;
    }
    @Override
    public double getPerimeter() {
        double perimeterSquare = 4 * x;
        return perimeterSquare;
    }
}
