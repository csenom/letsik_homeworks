public class Rectangle extends Figure {
    public Rectangle(int x, int y, int coordinateX, int coordinateY) {
        super(x, y);
    }
    @Override
    public double getPerimeter() {
        double perimeterRectangle = 2 * (x + coordinateY);
        return perimeterRectangle;
    }
}
