import java.util.List;

public class ArrayList<T> {
    private static final int DEFAULT_SIZE = 10;

    private T[] elements;
    private int size;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_SIZE];
        this.size = 0;
    }
    public void printArray(){
        this.elements = elements;
        for (int i = 0; i < size; i ++){
        System.out.println(this.elements[i]);
        }
    }


    public void add(T element) {
        // если массив уже заполнен
        if (isFullArray()) {
            resize();
        }
        this.elements[size] = element;
        size++;
    }

    private void resize() {

        T[] oldElements = this.elements;

        this.elements = (T[]) new Object[oldElements.length + oldElements.length / 2];
        // копируем все элементы из старого массива в новый
        for (int i = 0; i < size; i++) {
            this.elements[i] = oldElements[i];
        }
    }

    private boolean isFullArray() {
        return size == elements.length;
    }


    public T get(int index) {
        if (isCorrectIndex(index)) {
            return elements[index];
        } else {
            return null;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    public void clear() {
        this.size = 0;
    }
    public void size (){
        this.size = size;
        System.out.println(size);
    }

    public void removeAt(int index) {

        if(index < 0 || size <= index) {
            System.out.println("Такого индекса не существует");
            throw new IndexOutOfBoundsException(String.valueOf(index));
        }
        else {

            for (int i = index; i < size; i++) {
                elements[i]=elements[i + 1];
                if (elements[i]==null){
                    this.size = this.size -1;
                }

                //this.size --;

            }
        }
    }
}
