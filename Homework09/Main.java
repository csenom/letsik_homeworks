public class Main {

    public static void main(String[] args) {
	Figure figure = new Figure(10, 15);
        System.out.format("Возвращем 0: "+ "%.0f", figure.getPerimeter() );
        System.out.println();
    Square square = new Square(5);
        System.out.format("Периметр квадрата:" + "%.0f", square.getPerimeter());
        System.out.println();
    Ellipse ellipse = new Ellipse(10,15);
        System.out.format("Периметр элипса: " + "%.2f" ,ellipse.getPerimeter() );
        System.out.println();
    Rectangle rectangle = new Rectangle(10, 15);
        System.out.format("Периметр прямоугольника: " + "%.0f" , rectangle.getPerimeter());
        System.out.println();
    Circle circle = new Circle(10);
        System.out.format("Периметр круга: " +  "%.2f",circle.getPerimeter());
    }
}
