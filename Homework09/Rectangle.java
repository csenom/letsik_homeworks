public class Rectangle extends Figure{
    public Rectangle(int x, int y){
        super(x, y);
    }
    public double getPerimeter(){
        return 2 * (x + y);
    }
}
