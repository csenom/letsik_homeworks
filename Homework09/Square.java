public class Square extends Rectangle{
    public Square(int x){
        super(x,x);
    }
    public double getPerimeter(){
        return 4 * x;
    }
}
