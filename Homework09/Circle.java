public class Circle extends Ellipse{
    public Circle(int x){
        super(x,x);
    }
    public double getPerimeter(){
        double perimetrCircle;
        perimetrCircle = 2 * Math.PI * x;
        return perimetrCircle;
    }
}
