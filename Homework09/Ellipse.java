public class Ellipse extends Figure {
    public Ellipse(int x, int y) {
        super(x, y);
    }

    public double getPerimeter() {
        double a;
        double xSquare;
        double ySquare;
        double perimeterEllipse;
        xSquare  = x * x;
        ySquare = y * y;
        perimeterEllipse = (xSquare + ySquare) / 2;
        a = 2 * Math.PI * Math.sqrt(perimeterEllipse);
        return a;
    }
}
