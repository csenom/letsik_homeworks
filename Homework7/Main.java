import java.util.*;

public class Main {

    public static void main(String[] args) {
        int[] x = {10, 20, 30, 30, 30, 30, 40, 40, 40, 25};
        List<Integer> myInts = new ArrayList<Integer>();
        for (int index = 0; index < x.length; index++) {
            myInts.add(x[index]);
        }
        Map<Integer, Integer> ss = new HashMap<Integer, Integer>();
        for (int index = 0; index < x.length; index++) {
            if (Collections.frequency(myInts, x[index]) > 1) {
                ss.put(x[index], Collections.frequency(myInts, x[index]));
            }
        }
        Integer min = Collections.min(ss.values());
        for (Map.Entry<Integer, Integer> item : ss.entrySet()) {

            if (min == item.getValue()) {
                System.out.printf("В последовательности х минимально повторяющееся число:" +
                        " %d ,оно повторяется: %s раза\n", item.getKey(), item.getValue());
            }
        }
    }
}