import java.util.Arrays;
import java.util.Arrays;
public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {
        int[] arrayOut = new int[array.length];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                arrayOut[j] = array[i];
                ++j;
            }
        }
        return Arrays.copyOf(arrayOut,j);
    }

}
