import java.util.Arrays;
public class Main {
    public static void main(String[] args) {
        int[] array = {12, 14, 3, 13, 83, 465, 12, 387, 883, 99};
        int[] result = Sequence.filter(array, number -> number % 2 == 0);
        int[] result1 = Sequence.filter(array, number -> {
            int digitsSum = 0;
            boolean flag = false;
            while (number != 0) {
                int lastDigit = number % 10;
                digitsSum = digitsSum + lastDigit;
                number = number / 10;
            };
            if (digitsSum % 2 == 0) flag = true;

            return  flag;
        });
        System.out.println(Arrays.toString(result));
        System.out.println(Arrays.toString(result1));

    }
}
