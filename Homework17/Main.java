import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        String str = "This is it This This This ";

        String[] words = str.split(" ");

        Map<String, Integer> counterMap = new HashMap<>();
        for (String word : words) {
            if (!word.isEmpty()) {
                Integer count = counterMap.get(word);
                if (count == null) {
                    count = 0;
                }
                counterMap.put(word, ++count);
            }
        }

        for (String word : counterMap.keySet()) {
            if (counterMap.get(word) <= 1 ||counterMap.get(word) >= 5) {
                System.out.println("Слово " + word + ": " + "повторяется " + counterMap.get(word) + " раз");
            }
            else {
                System.out.println("Слово " + word + ": " + "повторяется " + counterMap.get(word) + " раза");
            }
        }
    }
}
