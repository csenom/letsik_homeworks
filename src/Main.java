import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите команду:");
        System.out.println("Add, Update, id");


        String str = scanner.next();
        str = str.toLowerCase(Locale.ROOT);
        str = str.replaceAll("[0-9]", "");
        str = str.trim();

        if (str.contains("id")) {
            System.out.println("Введите id");
            int id = scanner.nextInt();
            User user = (UsersRepositoryFileImpl.findById(id));
            System.out.println(user.getId() + " " + user.getName() + " "
                    + user.getAge() + " " + user.isWorker());
        }


        try {
            if (str.contains("add")) {
                System.out.println("Введите id");
                int id = scanner.nextInt();

                System.out.println("Введите Имя");
                String name = scanner.next();

                System.out.println("Введите Возраст");
                int age = scanner.nextInt();

                System.out.println("Работает человек?");
                String work = scanner.next();
                boolean workBoolean;


                name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();

                if (work.contains("Да")) {
                    workBoolean = true;
                } else {
                    workBoolean = false;
                }

                User user1 = new User(id, name, age, workBoolean);
                user1.setId(id);
                user1.setName(name);
                user1.setAge(age);
                user1.setWorker(workBoolean);

                System.out.println("Значение для id установлено: " + id + "\nИмя: "
                        + name + "\nВозраст: " + age + "\nРаботает ли человек: " + work);
                UsersRepositoryFileImpl.safe(user1);

            }
        } catch (Exception e) {
            System.err.println("При вводе ошибка");
        }


        try {
            if (str.contains("update")) {
                System.out.println("Введите id");
                int id = scanner.nextInt();

                System.out.println("Введите Имя");
                String name = scanner.next();

                System.out.println("Введите Возраст");
                int age = scanner.nextInt();

                System.out.println("Работает человек?");
                String work = scanner.next();
                boolean workBoolean;
                User user = (UsersRepositoryFileImpl.findById(id));

                name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
                user.setName(name);

                user.setAge(age);

                if (work.contains("Да")) {
                    workBoolean = true;
                } else {
                    workBoolean = false;
                }

                user.setWorker(workBoolean);

                UsersRepositoryFileImpl.findById(id);

                UsersRepositoryFileImpl.update(user);

                System.out.println("Значение для id установлено: " + id + "\nИмя: "
                        + name + "\nВозраст: " + age + "\nРаботает ли человек: " + work);
            }
        } catch (Exception e) {
            System.err.println("При вводе ошибка");
        }
    }
}



