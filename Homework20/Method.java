import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.Objects;
import java.util.OptionalDouble;

public class Method {
    public static void blackOrNewCar() {
        System.out.println("Номера черных машин или машин с нулевым пробегом");
        try (BufferedReader reader = new BufferedReader(new FileReader("cars.txt"))) {
            reader.lines().map(s -> {
                        String[] strings = s.split("\\|");
                        return new Car(strings[0], strings[1], strings[2], Integer.parseInt(strings[3]), Integer.parseInt(strings[4]));
                    })
                    .filter(car -> car.getCarMileage() == 0 || car.getCarColor().equals("Black"))
                    .map(Car::getCarNumber).forEach(System.out::println);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static void camryPrice() {
        try (BufferedReader reader = new BufferedReader(new FileReader("cars.txt"))) {
            OptionalDouble averagePrice = reader.lines().map(s -> {
                        String[] strings = s.split("\\|");
                        return new Car(strings[0], strings[1], strings[2], Integer.parseInt(strings[3])
                                , Integer.parseInt(strings[4]));
                    })
                    .filter(car -> car.getCarBrand().equals("Camry"))
                    .mapToDouble(Car::getCarPrice).average();
            if (averagePrice.isPresent()) {
                System.out.println("Средняя цена на Camry " + averagePrice.getAsDouble());
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static void uniqueModelsInThePriceRange() {
        try (BufferedReader reader = new BufferedReader(new FileReader("cars.txt"))) {
            long count = reader.lines()
                    .map(s -> {
                        String[] strings = s.split("\\|");
                        return new Car(strings[0], strings[1], strings[2], Integer.parseInt(strings[3]),
                                Integer.parseInt(strings[4]));
                    })
                    .filter(car -> car.getCarPrice() >= 700 && car.getCarPrice() <= 800)
                    .map(Car::getCarBrand).distinct().count();
            System.out.println("Количество разных моделей ценой 700-800 тыс. - " + count);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
    public static void colorMinPrice() {
        try (BufferedReader reader = new BufferedReader(new FileReader("cars.txt"))) {
            String minPrice = reader.lines()
                    .map(s -> {
                        String[] strings = s.split("\\|");
                        return new Car(strings[0], strings[1], strings[2],
                                Integer.parseInt(strings[3]), Integer.parseInt(strings[4]));
                    })
                    .min(Comparator.comparingInt(Car::getCarPrice))
                    .map(Car::getCarColor)
                    .map(Objects::toString)
                    .orElse("");
            System.out.println("Цвет машины с минимальной ценой - " + minPrice);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
