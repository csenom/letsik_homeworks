public class Car {
    //[НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]
    private String carNumber;
    private String carBrand;
    private String carColor;
    private int carMileage;
    private int carPrice;

    public Car(String carNumber,String carBrand,String carColor, int carMileage, int carPrice) {
        this.carNumber = carNumber;
        this.carMileage = carMileage;
        this.carPrice = carPrice;
        this.carBrand = carBrand;
        this.carColor = carColor;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber() {
        this.carNumber = carNumber;
    }

    public int getCarMileage() {
        return carMileage;
    }

    public void setCarMileage() {
        this.carMileage = carMileage;
    }

    public int getCarPrice() {
        return carPrice;
    }

    public void setCarPrice() {
        this.carPrice = carPrice;
    }
    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand() {
        this.carBrand = carBrand;
    }
    public String getCarColor() {
        return carColor;
    }

    public void setCarColor() {
        this.carColor = carColor;
    }
}
