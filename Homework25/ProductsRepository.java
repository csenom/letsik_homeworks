import java.util.List;

public interface ProductsRepository {
    List<Product> findAll();
    void save(Product product);
    List<Product> findByAge(int age);
    List<Product> findAllByPrice(double price);

}
