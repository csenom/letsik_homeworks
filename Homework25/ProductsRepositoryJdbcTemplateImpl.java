import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {

    private static final String SQL_INSERT = "insert into product (description,price,count) values (?, ?, ?)";

    private static final String SQL_SELECT_ALL_BY_PRICE = "select * from product where count = 15000";

    private JdbcTemplate jdbcTemplate;

    private static final String SQL_SELECT_ALL = "select * from product order by id";

    private static  final RowMapper<Product> productRowMapper = (row, rowNum) ->{
        Product product = new Product();
        int id = row.getInt("id");
        String description = row.getString("description");
        int price = row.getInt("price");
        int count = row.getInt("count");

        return new Product(id,description,price,count);
    } ;




    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }


    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public void save(Product product) {
    jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getCount(), product.getPrice());
    }

    @Override
    public List<Product> findByAge(int age) {
        return null;
    }

    @Override
    public List<Product> findAllByPrice(double price) {
       return jdbcTemplate.query(SQL_SELECT_ALL_BY_PRICE, productRowMapper);

    }


}
