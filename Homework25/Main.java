import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/postgres"
                , "postgres", "Qwerty123");
        ProductsRepository productsRepository = new ProductsRepositoryJdbcTemplateImpl(dataSource);
        Product product = Product.builder().description("AirPods").price(15000).count(3).build();
        productsRepository.save(product);
        System.out.println(productsRepository.findAll());

        System.out.println(productsRepository.findAllByPrice(35000));


    }
}
