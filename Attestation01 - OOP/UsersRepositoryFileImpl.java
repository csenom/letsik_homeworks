import java.io.*;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class UsersRepositoryFileImpl {

    public static ArrayList<User> users;

    public static User findById(int id) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("Homework19/users.txt"))) {

            users = bufferedReader.lines()
                    .map(line -> line.split("\\|"))
                    .map(l -> new User(Integer.parseInt(l[0]), l[1],
                            Integer.parseInt(l[2]), Boolean.parseBoolean(l[3])))
                    .collect(Collectors.toCollection(ArrayList::new));

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
        return users.stream().filter(user -> user.getId() == id)
                .findFirst().orElse(null);
    }

    public static void update(User user) {
        Writer writer;
        BufferedWriter bufferedWriter;

        if (user.getAge() < 0 || user.getAge() > 150) {
            System.out.println("Проверьте возраст");
            throw new IllegalArgumentException();
        } else {
            try {
                writer = new FileWriter("Homework19/users.txt");
                bufferedWriter = new BufferedWriter(writer);

                users.remove(user.getId() - 1);
                users.add(user.getId() - 1, user);

                for (User newUser : users) {
                    bufferedWriter.write(newUser.getId() + "|" + newUser.getName() + "|" + newUser.getAge() + "|" + newUser.isWorker());
                    bufferedWriter.newLine();
                    bufferedWriter.flush();
                }
            } catch (IOException e) {
                throw new IllegalArgumentException();
            }
        }
    }

    public static void safe(User user) {
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter("Homework19/users.txt", true);
            bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write(user.getId() + "|" + user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {
                }
            }
        }
    }
}
