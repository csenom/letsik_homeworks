import java.util.List;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("Homework19/users.txt");
        List<User> usersWork = usersRepository.findByIsWorkerIsTrue();
        List<User> usersAll = usersRepository.findAll();
        List<User> usersAge = usersRepository.findByAge(31);
        for (User user : usersAll) {
            System.out.println("Распечатем весь список " +
                    user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
        System.out.println("///////////////////////");
        for (User user : usersAge) {
            System.out.println("Распечатем по количеству лет " +
                    user.getAge() + " " + user.getName());
        }
        System.out.println("///////////////////////");
        for (User user : usersWork) {
            System.out.println("Распечатем всех кто работает " + user.getName() + " " + user.isWorker());
        }

        /*User user = new User("Игорь", 33, true);
        usersRepository.save(user);*/




    }
}
