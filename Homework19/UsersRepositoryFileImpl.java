import javax.imageio.IIOException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();

        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {

            reader = new FileReader(fileName);

            bufferedReader = new BufferedReader(reader);

            String line = bufferedReader.readLine();

            while (line != null) {
                String[] parts = line.split("\\|");

                String name = parts[0];

                int age = Integer.parseInt(parts[1]);

                boolean isWorker = Boolean.parseBoolean(parts[2]);

                User newUser = new User(name, age, isWorker);

                users.add(newUser);

                line = bufferedReader.readLine();
            }



            if (reader != null) {


                    reader.close();

            }
            if (bufferedReader != null) {


                    bufferedReader.close();

            }
        }catch (Exception e){
            System.err.println("В коде ошибка");
        }

        return users;
    }

    @Override
    public void save(User user) {
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(fileName, true);
            bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write(user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {}
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {}
            }
        }
    }

    @Override
    public List<User> findByAge(int age) {
        List<User> findAge = new ArrayList<>();
        for (User user : findAll()) {
            if (user.getAge() == age) {
                findAge.add(user);
            }
        }
        return findAge;

    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        List<User> findWorker = new ArrayList<>();
        for (User user : findAll()) {
            if (user.isWorker() == true ) {
                findWorker.add(user);
            }
        }
        return findWorker;
    }
}
