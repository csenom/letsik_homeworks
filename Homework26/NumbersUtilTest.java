import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName(value = "NumberUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {

    private final NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName("maximalDivider() is working")
    public class maximalDivider {

        @ParameterizedTest(name = "throws exception on {0}")
        @CsvSource(value = {"0, 12, throws exception", "50 , 0 , throws exception"})
        public void bad_numbers_throws_exception(int a, int b) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "return {2} on {0} / {1} and vice versa")
        @CsvSource(value = {"18, 12, 6", "50 , 75 , 25", "90, 36, 18"})
        public void maximalDivider1(int a, int b, int result) {
            assertEquals(result, numbersUtil.gcd(a, b));
        }
    }
}



