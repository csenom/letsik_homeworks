public class NumbersUtil {

    private int k;
    private int a;
    private int b;

    public int gcd(int a, int b) {

        if (a == 0 || b == 0 || a < 0 || b < 0)
            throw new IllegalArgumentException();

        while ( a > 0) {
            for (int i = 1; i <= a; i++) {
                if (a % i == 0) {
                    if ( b % i == 0){
                        k = i;
                    }
                }
            } return k;
        } return -1;
    }
}
